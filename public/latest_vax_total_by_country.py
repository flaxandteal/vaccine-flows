import pandas as pd

df = pd.read_csv(r'/home/ronan/Git/Viridian/vaccine-flows/public/data.csv')
df = df.drop_duplicates('iso_code', keep='last')
df.to_csv(r'/home/ronan/Git/Viridian/vaccine-flows/public/data2.csv')