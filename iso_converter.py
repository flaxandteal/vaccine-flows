import pandas as pd

countryCodes = pd.read_csv(r'/home/ronan/Git/Viridian/vaccine-flows/public/country-codes.csv')
links = pd.read_csv(r'/home/ronan/Git/Viridian/vaccine-flows/public/links.csv')
countryCodes2 = countryCodes['ISO3166-1-Alpha-2']
countryCodes = countryCodes['ISO3166-1-Alpha-3']

for i in range(0, len(links)):
    for j in range(0, len(countryCodes) ):
        if (links.loc[i,'target']) == countryCodes.iloc[j]:
            links.loc[i,'target'] = countryCodes2.iloc[j]

links.to_csv(r'/home/ronan/Git/Viridian/vaccine-flows/public/links.csv')
            

