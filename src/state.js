class State {
  constructor (definition) {
    this.definition = definition;
    this.data = definition.links;
  }

  get total () {
    return Object.values(this.data)
  }
}

export default { State };
