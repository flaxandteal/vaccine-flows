import Vue from 'vue'
import Vuex from 'vuex'
import iso3166 from 'iso-3166-1'
import axios from 'axios'
import { spatialsankey } from 'd3-spatialsankey'
import * as d3base from 'd3'
const d3 = Object.assign(d3base, { spatialsankey })

Vue.use(Vuex)

// To move to class
function totalForState(state, inward, ste, own) {
  const val = Object.entries(state.flow)
    .filter(lp => (own || ste !== lp[0]))
    .map(lp => inward && ste !== lp[0] ? -lp[1].value : lp[1].value)
    .filter(l => l > 0)
    .reduce((a, l) => a + l, 0);
  return val;
}

const store = new Vuex.Store({
  state: {
    totalForState,
    year: 2021,
    autoAddReverseLinks: true,
    inward: true,
    own: false,
    ste: "",
    isSelected: false,
    nodes: {},
    links: [],
    codes: null,
    divisions: {},
    filter: {},
    articles: {},
    states: {},
    population: null,
    casesAndDeaths: null,
    vacManufacLocs: null,
    vaxTotalByCountry: null,
    GaNames: null
  },
  mutations: {
    toggleOwn (state) {
      state.own = !state.own;
    },
    toggleInward (state) {
      state.inward = !state.inward;
    },
    setFilter (state, filter) {
      state.filter = filter;
    },
    setStates (state, states) {
      state.states = states;
    },
    setSte (state, ste) {
      state.ste = ste;
    },
    setIsSelected (state, isSelected) {
      state.isSelected = isSelected;
    },
    setData(state, { nodes, links }) {
      state.nodes = nodes;
      state.links = links;
    },
    setDivisions(state, divisions) {
      state.divisions = divisions;
    },
    setCodes(state, codes) {
      state.codes = codes;
    },
    setArticles(state, articles) {
      state.articles = articles;
    },
    setCountryPopulation(state, population) {
      state.population = population;
    },
    setCasesAndDeaths(state, casesAndDeaths) {
      state.casesAndDeaths = casesAndDeaths
    },
    setVacManufacLocs(state, vacLocs) {
      var loc;
      var locs = [];
      for (loc in vacLocs){
        locs.push({'geometry': {'coordinates' : [ (vacLocs[loc]["manufacturer_latitude"]), (vacLocs[loc]["manufacture_longitude"]) ], 'type': "Point"},
        'countries_supplied': vacLocs[loc]['countries_supplied'],
        'id': vacLocs[loc]['manufacturing_iso_code'],
        'properties' : {'LAT': (vacLocs[loc]["manufacturer_latitude"]), 'LON': (vacLocs[loc]["manufacture_longitude"])}, 
        'type': "Feature"})
      }
      state.vacManufacLocs = locs
    },
    setVaxTotalByCountry(state, vaxByCountry) { 
      state.vaxTotalByCountry = vaxByCountry
    },
    setGaNames(state, GaNames) {
      state.GaNames = GaNames
    }
  },
  actions: {
    loadArticles: function ({ commit }) {
        return commit('setArticles', []);
    },
    loadDivisions: function ({ commit }) {
      return d3.csv("divisions.csv").then(function(divisions) {
        const structuredDivisions = divisions.reduce((acc, division) => {
          acc[division.code] = division.breakdown.split('|').reduce((acc2, entry) => {
            const pair = entry.split(':');
            acc2[pair[0]] = parseFloat(pair[1]);
            return acc2;
          }, {});
          return acc;
        }, {});
        commit('setDivisions', structuredDivisions);
      });
    },
    loadGaNames: function ({commit}) {
      return d3.csv("countries-ga-url.csv").then(function(ga_codes){
        return commit('setGaNames', ga_codes)
      }, {}) 
    },

    loadCodes: function ({ commit }) {
      return d3.csv("countries-ga-url.csv").then(function(ga_codes) {
        var gaCodeMap = ga_codes.reduce(function (gaCodeMap, code) {
          gaCodeMap[code['ISO-3166-1-alpha2']] = code;
          return gaCodeMap;
        }, {});

        return d3.csv("country-codes.csv").then(function(codes) {
          var codeMap = codes.reduce(function (codeMap, code) {
            var cc = code['ISO3166-1-Alpha-2'];
            codeMap[cc] = code;
            var cc3 = code['ISO3166-1-Alpha-3'];
            codeMap[cc3] = code;
            if (cc in gaCodeMap) {
              codeMap[cc].dfaie = gaCodeMap[cc];
            }
            return codeMap;
          }, {});
          codeMap['UK'] = codeMap['GB'];
          codeMap['UK'].dfaie = gaCodeMap['UK'];
          commit('setCodes', codeMap);
          return codeMap;
        })
      })
    },
    loadData: function ({ commit, state, dispatch }) {
      return dispatch('loadDivisions').then(() => d3.json('nodes.geojson')).then(function(nodes) {
        return d3.csv('links.csv').then(function(links) {
          var fillData = function (codes) {
            const divisionLinks = links.reduce((dL, link) => {
              if (link.target in state.divisions) {
                Object.entries(state.divisions[link.target]).forEach(participant => {
                  dL.push({
                    target: participant[0],
                    source: link.source,
                    urls: link.urls,
                    flow: Math.round(parseInt(link.flow) * participant[1])
                  })
                });
              }
              return dL;
            }, []);
            links = links.concat(divisionLinks);
            const divisionLinksOut = links.reduce((dL, link) => {
              if (link.source in state.divisions) {
                Object.entries(state.divisions[link.source]).forEach(participant => {
                  dL.push({
                    target: link.target,
                    source: participant[0],
                    urls: link.urls,
                    flow: Math.round(parseInt(link.flow) * participant[1])
                  })
                });
              }
              return dL;
            }, []);
            links = links.concat(divisionLinksOut);

            const allAppearingStates = {};
            const allLinks = [];
            console.log(state.own);
            var matchingLinks = links.filter(
              link => state.own || link.target !== link.source
            ).reduce((states, link) => {
              if (!(link.source in codes) || !(link.target in codes)) {
                return states;
              }
              if (link.urls) {
                link.urls = link.urls.split('|');
              }
              if (!(link.source in states)) {
                states[link.source] = {};
              }

              let value = parseInt(link.flow);
              let urls = link.urls;
              if (link.target in states[link.source]) {
                value += states[link.target][link.source].value;
                if (states[link.target][link.source].urls) {
                  if (urls) {
                    urls = urls.concat(states[link.target][link.source].urls);
                  } else {
                    urls = states[link.target][link.source].urls;
                  }
                }
              }

              states[link.source][link.target] = {
                value: value,
                urls: urls
              };
              allAppearingStates[link.source] = true;
              allAppearingStates[link.target] = true;
              allLinks.push(link);

              if (state.autoAddReverseLinks && link.target != link.source) {
                if (!(link.target in states)) {
                  states[link.target] = {};
                }
                let value = -parseInt(link.flow);
                let urls = link.urls;
                if (link.source in states[link.target]) {
                  value += states[link.target][link.source].value;
                  if (states[link.target][link.source].urls) {
                    if (urls) {
                      urls = urls.concat(states[link.target][link.source].urls);
                    } else {
                      urls = states[link.target][link.source].urls;
                    }
                  }
                }
                states[link.target][link.source] = {
                  value: value,
                  urls: urls
                };
                allLinks.push({
                  source: link.target,
                  target: link.source,
                  flow: -parseInt(link.flow),
                  urls: link.urls
                });
              }

              return states;
            }, {});

            var states = {};
            console.log(allAppearingStates);
            states = nodes.features.reduce((states, node) => {
              if (node.id in codes && node.id in allAppearingStates) {
                 states[node.id] = {
                   code: codes[node.id],
                   node: node,
                   flow: node.id in matchingLinks ? matchingLinks[node.id] : { value: 0 }
                 };
              }
              return states;
            }, states);

            commit('setData', { nodes, links: allLinks })
            commit('setStates', states);

            return states;
          };

          if (state.codes) {
            fillData(state.codes);
          } else {
            return dispatch('loadCodes').then(fillData);
          }

          return { nodes, links }
        })
      })
    },

    loadPopulation: function ( {commit} ) {
      return d3.csv('Population_By_Country.csv').then(function(population) {
        return commit('setCountryPopulation', population)
      });
    },

    loadCasesAndDeaths: function ( {commit} ) {
      axios
      .get('https://corona-api.com/countries')
      .then(response => (
        commit('setCasesAndDeaths', response.data['data'])))
    },
    loadManufacLocs: function( {commit} ) {
      return d3.csv('vaccination_locations.csv').then(function(vacLocs) {
        return commit('setVacManufacLocs', vacLocs)
      });
    },
    loadVaxTotalByCountry: function( {commit} ) {
      return d3.csv('data2.csv').then(function(vaxByCountry) {
        return commit('setVaxTotalByCountry', vaxByCountry)
      });
    }
  },
  getters: {
    statesByFlow: function (state) {
      var filter = state.filter;
      var states = state.states;
      var filteredStates = Object.keys(states).filter(function (ste) {
        if (ste in state.states) {
          for (var key in filter) {
            if (states[ste].code[key] !== filter[key]) {
              return false;
            }
          }
          return true;
        }
      });

      return filteredStates.reduce(function (list, ste) {
        const sum = totalForState(states[ste], state.inward, ste, state.own);
        list.push([ste, sum]);
        return list;
      }, []).filter(pair => pair[1] > 0).sort(function (a, b) {
        return (b[1] - a[1]);
      }).map(p => p[0]);
    },
    hasSelectedState: function (state) {
      return state.ste.length && state.isSelected
    },
    steCode: function (state) {
      if (state.ste.length && state.codes) {
        return state.codes[state.ste.toUpperCase()]
      }
      return null
    },
    steItem: function () {
      return ste => {
        if (ste.length) {
          const alpha2 = ste.toLowerCase()
          if (alpha2 === 'uk') {
            return iso3166.whereAlpha2('gb')
          }
          return iso3166.whereAlpha2(alpha2)
        }
        return null
      }
    },
    steArticles: function (state) {
      if (state.ste.length && state.ste in state.articles) {
        return state.articles[state.ste]
      }
      return []
    },
    dosesByState: function (state) {
      if (state.ste && state.states[state.ste]) {
        return Object.entries(state.states[state.ste].flow).map(entry => ({
          state: state.states[entry[0]].code,
          flow: entry[1].value * (state.inward && state.ste !== entry[0] ? -1 : 1),
          urls: entry[1].urls
        })).filter(doses => doses.flow > 0);
      }
    },
    totalPopulation: function (state) {
      if (state.ste && state.states[state.ste]) {
        return totalForState(state.states[state.ste], state.inward, state.ste, state.own);
      }
    },
    direction: function (state) {
      return state.inward ? 'from' : 'to';
    }
  }
})

export default store
